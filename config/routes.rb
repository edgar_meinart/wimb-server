WhereIsMyBusServer::Application.routes.draw do

  get "city/index"

  get "city/show"

  get "transports/show"

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  namespace :api do
    post "create_broadcast" => "v1::api#create_broadcast"
    put "update_broadcast" => "v1::api#update_broadcast"


    get "status" => "v1::api#status"
    

    post "update_user" => "v1::api#update_user"
    post "create_watchdog_checkpoint" => "v1::api#create_watchdog_checkpoint"
    put "update_watchdog_checkpoint" => "v1::api#update_watchdog_checkpoint"
    delete "destroy_watchdog_checkpoint" => "v1::api#destroy_watchdog_checkpoint"

    get "get_basic_info_about_user" => "v1::api#get_basic_info_about_specefic_user"
    get "get_basic_info_about_specefic_watchdog" => "v1::api#get_basic_info_about_specefic_watchdog"
    get "get_basic_info_about_specefic_broadcats" => "v1::api#get_basic_info_about_specefic_broadcast"
    get "get_basic_info_about_specefic_city" => "v1::api#get_basic_info_about_specefic_city"

    get "say_warning" => "v1::api#say_warning"
    get "say_thanks" => "v1::api#say_thanks"

    get "get_watchdog_list" => "v1::api#get_watchdog_list"
    get "broadcasts" => "v1::api#broadcasts"
  end
  
  root to: "home_page#index"


  get "/review", to: "reviews#new", as: "review"
  post "/post", to: "reviews#create", as: "post_review"
  get "/search", to: "home_page#search"
  
  
  match "howitworks", to: "home_page#tour", as: "howitworks"
  match "terms", to: "home_page#terms", as: "terms"
  match "support", to: "home_page#support", as: "support"

  get "cities", to: "city#index", as: "cities"
  get "cities/:url_type", to: "city#show", as: "city"

  get '/top', to: "users#top", as: "user_top"
  get '/:phone_number', to: "users#show", as: :user
  get '/:phone_number/transports', to: "users#transports", as: "user_transports"
  

  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
