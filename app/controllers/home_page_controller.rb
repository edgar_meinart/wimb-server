class HomePageController < ApplicationController
  def index
  	Sidekiq::Client.enqueue(UpdateUserTopPosition)
  	Sidekiq::Client.enqueue(UpdateUserTopPositionLocal,{location: @current_city})
  	Sidekiq::Client.enqueue(UpdateUserTopPositionLocal,{location: "Moscow"})
  end

  def terms

  end

  def tour
    
  end

  def support
    
  end

  def search
    if params[:search].present?
      users = User.where('$or' => [ {:username => params[:search][:data]}, {:email => params[:search][:data]}, {:phone_number => params[:search][:data]} ])
      @result = []
      users.each do |user|
        @result << {username: user.username, phone_number: user.phone_number}
      end
    end
  end
  
end
