class ApplicationController < ActionController::Base
	protect_from_forgery
	before_filter :set_location_language
	before_filter :current_city


	def current_city
		city = ""
		Rails.env == "development" ? city = "Riga" : city = request.location.city
		@current_city = city
	end


	def set_location_language
		I18n.locale = "lv"
	end


protected
	def check_register_new_user
		if params[:device_id].present?
			begin
			@client = User.find_by(device_id: params[:device_id]) 
			rescue
				p "CREATE USER WITH DEVICE ID: #{params[:device_id]}"
				@client = User.create(device_id: params[:device_id]) 
				city = params[:city].nil? ? "unknown" : params[:city]
				Sidekiq::Client.enqueue(UpdateCitiesList, {users_count: true, city: city})
			end	
		end
	end

end
