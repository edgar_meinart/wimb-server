class UsersController < ApplicationController
	def index
		
	end
	def show
		@user = User.find_by(phone_number: params[:phone_number].downcase)
		@user_count_local = User.where(city: @user.city).size
		@user_count_world = User.all.size
	end

	def transports
		@user = User.find_by(phone_number: params[:phone_number])
	end

	def top
		@world_tab_active = ""
		@local_tab_active = "active"
		if params[:location] != "world"
			@top_title = "#{ t 'user_top_in_city'} #{params[:location]}"
			@world_tab_active = ""
			@local_tab_active = "active"
			@users = User.where(city: params[:location],take_part_in_the_top: true).desc(:rank).limit(10)

		elsif params[:location] == "world"
			@top_title = "#{t 'user_top_in_world'}"
			@world_tab_active = "active"
			@local_tab_active = ""
			@users = User.where(take_part_in_the_top: true).desc(:rank).limit(10)
		else
			render nothing: true, status: 404
		end
	end

	def world_top
		@users = User.all.desc(:rank).limit(10)
	end



end
