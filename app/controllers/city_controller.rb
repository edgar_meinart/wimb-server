class CityController < ApplicationController
  def index
  	@cities = City.all
  end

  def show
  	@city = City.find_by(url_type: params[:url_type])
  end
end
