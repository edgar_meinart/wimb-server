class ReviewsController < ApplicationController
	def new
		@categories = [{name:"categories.bug_report", id: 0}, {name:"categories.advice", id: 1}]
		render "reviews/new.html"
	end

	def create
		# if !verify_recaptcha
		UserMailer.review_mail(params).deliver
		flash[:notice] = t 'review_poster'
		respond_to do |format|
		    format.html { render 'reviews/posted.html'}
		end
	end
end
