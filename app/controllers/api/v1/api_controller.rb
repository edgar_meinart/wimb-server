class Api::V1::ApiController < ApplicationController
  #before_filter :verify_user_credentials, only: [:create_broadcast, :update_broadcast, :create_transport, :create_watchdog_checkpoint]
  before_filter :check_register_new_user

  def update_user
    @client.update_attributes(params)
    render json: {status: "updated"}, status: :ok
  end


  def create_broadcast
    user_broadcast = Broadcast.new(transport_type: params[:transport_type], transport_number: params[:transport_number], lat: params[:lat],lon: params[:lon], city: params[:city])
    @client.broadcast << user_broadcast 
    activity = Activity.new(object: @client.class.to_s, subject: user_broadcast.class.to_s, data: {city: params[:city], transport_type: params[:transport_type], transport_number: params[:transport_number] })
    @client.activities << activity
    Sidekiq::Client.enqueue(UpdateCitiesList, {broadcasts_count: true, transports_count: true, city: params[:city], transport: "#{params[:transport_type]},#{params[:transport_number]}"})
    render json: {broadcast_id: user_broadcast.id}
  end

  def update_broadcast
  	Sidekiq::Client.enqueue(UpdateBroadcast, params)
  	puts "#{params}"
  	render nothing: true, status: 200
  end	
  

  def create_watchdog_checkpoint
    watchdog = Watchdog.create!(lat: params[:lat], lon: params[:lon], type: params[:type], message: params[:message], city: params[:city])
    Sidekiq::Client.enqueue(UpdateCitiesList, {warnings_count: true, city: params[:city]})
    render json: {watchdog_id: watchdog.id}, status: :ok
  end

  def update_watchdog_checkpoint
    
  end

  def destroy_watchdog_checkpoint
    
  end

  def get_basic_info_about_specefic_user
    @user = User.find(params[:id])
    render json: @user, status: 200 if !@user.nil?
    render nothing: true, status: 404 if @user.nil?
  end

  def  get_basic_info_about_specefic_broadcast
    @broadcast = Broadcast.find(params[:id])
    render json: @broadcast, status: 200 if !@broadcast.nil?
    render nothing: true, status: 404 if @broadcast.nil?
  end

  def  get_basic_info_about_specefic_watchdog
    @watchdog = Watchdog.find(params[:id])
    render json: @watchdog, status: 200 if !@watchdog.nil?
    render nothing: true, status: 404 if @watchdog.nil?
  end

  def  get_basic_info_about_specefic_city
    limit = params[:limit].present? ? params[:limit].to_i : 10
    info_list = []

    info_list += User.where(city: params[:city]).limit(limit)
    info_list += Broadcast.where(city: params[:city]).limit(limit)
    info_list += Watchdog.where(city: params[:city]).limit(limit)
    render json: info_list,status: 200
  end

  def say_thanks
    if params[:subject].present?
      Sidekiq::Client.enqueue(SayThanks, {subject: params[:subject].to_s, object: @client.id.to_s})
      render nothing: true, status: 200
    else
      render json: {message: "need subject id!"}, status: 500
    end
  end

  def say_warning
    if params[:subject].present?
      Sidekiq::Client.enqueue(SayWarning, {subject: params[:subject].to_s})
      render nothing: true, status: 200
    else
      render json: {message: "need subject id!"}, status: 500
    end
  end

  def status
  	render json: {status: "ok",data: params}, status: 200
  end

  def broadcasts
    limit = params[:limit].present? ? params[:limit].to_i : 10
  	b = nil
    if params[:city].present? && params[:transport_type].present? && params[:transport_number].present?
      b = Broadcast.where(city: params[:city], transport_type: params[:transport_type], transport_number: params[:transport_number]).limit(limit)
    elsif params[:city].present? && params[:transport_type].present?
      b = Broadcast.where(city: params[:city], transport_type: params[:transport_type]).limit(limit)
  	elsif params[:city].present?
  		b = Broadcast.where(city: params[:city]).limit(limit)
  	else
      b = Broadcast.all.limit(limit)
    end
    render json: {broadcasts: b}, status: :ok
  end

  def get_watchdog_list
    limit = 10
    watchdogs = Watchdog.where(city: params[:city]).limit(limit)
    render json: watchdogs, status: :ok
  end

end
