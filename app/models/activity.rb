class Activity
  include Mongoid::Document
  field :object, type: String
  field :subject, type: String
  field :data, type: Hash

  embedded_in :user
end
