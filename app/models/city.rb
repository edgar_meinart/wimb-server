class City
  include Mongoid::Document
  
  field :caption, type: String, default: ""
  validates :caption, uniqueness: true

  field :url_type, type: String, default: ""
  index({url_type: 1}, {unique: true} )
  validates :url_type, uniqueness: true


  field :users_count, type: Integer, default: 0
  field :transports_count, type: Integer, default: 0
  field :transports, type: Array, default: []
  field :broadcasts_count, type: Integer, default: 0
  field :warnings_count, type: Integer, default: 0

  before_save :convert_to_url_type



  def to_param
    url_type
  end

  private
  
  def convert_to_url_type
  	self.url_type = self.caption.downcase.gsub(' ','-')
  end

end
