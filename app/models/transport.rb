class Transport
  include Mongoid::Document
  include Mongoid::Timestamps
  field :number, type: String
  field :type, type: String
  field :city, type: String
  field :organization, type: String
  field :route, type: String

  belongs_to :user
end
