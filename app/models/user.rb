class User
  include Mongoid::Document

  ## Database authenticatable

  field :username,        :type => String, :default => ""
  field :device_id,       :type => String, :default => ""
  index( {device_id: 1, username: 1 },{ unique: true })

  field :phone_number,   :type => String, :default => "" 
  index({phone_number: 1}, {unique: true} )

  field :language,           :type => String, :default => ""
  field :email,              :type => String, :default => ""
  field :city,               :type => String, :default => ""
  
  ## Recoverable
  field :reset_password_token,   :type => String
  field :reset_password_sent_at, :type => Time

  ## Trackable
  field :sign_in_count,      :type => Integer, :default => 0
  field :current_sign_in_at, :type => Time
  field :last_sign_in_at,    :type => Time
  field :current_sign_in_ip, :type => String
  field :last_sign_in_ip,    :type => String

  field :rank, type: Integer, :default => 0
  index({rank: -1}, {unique: false} )
  
  field :position_in_top_world, type: Integer
  field :position_in_top_local, type: Integer
  before_create { self.position_in_top_local = User.all.size and self.position_in_top_world = User.all.size}

  field :thanks_count, type: Integer, default: 0
  field :say_thanks_count, type: Integer, default: 0
  field :warnings_count, type: Integer, default: 0


  field :take_part_in_the_top, type: Boolean, default: true

  has_many :transports
  has_many :broadcast, dependent: :destroy
  #has_many :activities, dependent: :destroy

  embeds_many :activities
  def to_param
    phone_number.to_s
  end


end
