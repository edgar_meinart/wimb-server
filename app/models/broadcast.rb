class Broadcast
  include Mongoid::Document
  include Mongoid::Timestamps

  belongs_to :user

  field :city, type: String
  field :lat, type: String
  field :lon, type: String
  field :last_update_date, type: Time
  field :transport_type, type: String
  field :transport_number, type: String
end
