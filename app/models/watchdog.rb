class Watchdog
  include Mongoid::Document
  field :lat, type: Float
  field :lon, type: Float
  field :type, type: String
  field :message, type: String
  field :city, type: String
end
