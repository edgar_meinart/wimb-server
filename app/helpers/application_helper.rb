module ApplicationHelper
	def get_current_position
		return @current_city
  	end

  	def get_current_language
  		return t "language.#{I18n.locale}"
  	end
end
