class UpdateUserTopPositionLocal
	include Sidekiq::Worker
	@queue = :update_local_top_position


	def perform(options={})
	users = User.where(phone_number: options["location"], take_part_in_the_top: true).desc(:rank)
		users.all.each_with_index do |user,index|
			user.update_attribute(:position_in_top_local, index + 1 )
		end
	end
end