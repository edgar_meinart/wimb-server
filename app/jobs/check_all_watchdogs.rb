class CheckAllWatchdogs
	include Sidekiq::Worker
	@queue = :check_all_watchdogs


	def perform(options={})
		if options["city"].present?
			watchdog = Watchdog.where(city: options["city"])
			watchdog.each do |w|
				if (Time.now - w.created_at) > 7200 #or 2 hours
					w.destroy
				end
			end
		end
		
	end

end