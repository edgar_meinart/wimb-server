class CreateWatchdog
	include Sidekiq::Worker
	@queue = :create_watchdog


	def perform(options={})
		puts options
		puts "create watchdog"
		watchdog = Watchdog.create(lat: options["lat"], lon: options["lon"], city: options["city"], type: options["type"], message: options["message"])
		#
		Sidekiq::Client.enqueue(CheckAllWatchdogs, {city: options["city"]})


		if options["user_id"].present?
			begin
				user = User.find(options["user_id"])
				activity = Activity.new(type: watchdog.class.to_s, data: {city: options["city"]} )
				user.activities << activity
			rescue
				puts "error! #{self.class.to_s}"
			end
		end
	end

end