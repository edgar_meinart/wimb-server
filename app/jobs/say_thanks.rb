class SayThanks
	include Sidekiq::Worker
	@queue = :say_thanks


	def perform(options={})
		if options["subject"].present?
			p options["subject"]
			p options["object"]
			subject = User.find(options["subject"].to_s)
			object = User.find(options["object"].to_s)
			subject.inc(:thanks_count, 1)
			object.inc(:say_thanks_count, 1) 
		end
	end

end