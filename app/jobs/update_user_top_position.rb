class UpdateUserTopPosition
	include Sidekiq::Worker
	@queue = :update_top_position


	def perform
		users = User.where(take_part_in_the_top: true).desc(:rank)
		users.all.each_with_index do |user,index|
			user.update_attribute(:position_in_top_world, index + 1 )
		end
	end

end