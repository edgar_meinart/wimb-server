class UpdateBroadcast
	include Sidekiq::Worker
	@queue = :update_broadcast


	def perform(options={})
		begin
			puts options
			puts "Joining broadcasts for #{options["user_broadcast_id"]}"
			user_broadcast = Broadcast.find(options["user_broadcast_id"])
	  		user_broadcast.update_attributes({lat: options["lat"], lon: options["lon"]})


		    if options["destroy"].to_s == "true"
		    	created_at = user_broadcast.created_at
		    	time_now = Time.now
		    	rank = (time_now - created_at).to_i / 60
		    	user_broadcast.user.inc(:rank, rank) if rank >= 1
		        user_broadcast.destroy
		       	p " Broadcast destroyed "
			end
		rescue
			puts "=--------------------ERROR-----------------------"
		end
	end

end