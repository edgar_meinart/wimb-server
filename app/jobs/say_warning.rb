class SayWarning
	include Sidekiq::Worker
	@queue = :say_warning


	def perform(options={})
		if options["subject"].present?
			subject = User.find(options["subject"].to_s)
			subject.inc(:warnings_count, 1)
		end
	end

end