class UpdateCitiesList
	include Sidekiq::Worker
	@queue = :update_cities_list


	def perform(options={})
		city_url_type = options["city"].downcase.gsub(" ","-")
		reason = options["reason"]
		city = City.create(caption: options["city"])

		
		city = City.where(url_type: city_url_type).first
		city = City.create(caption: options["city"]) if city.nil?

		city.inc(:broadcasts_count,1) if options["broadcasts_count"] == true
		city.inc(:warnings_count,1) if options["warnings_count"] == true
		city.inc(:users_count,1) if options["users_count"] == true

		if options["transports_count"] == true
			if !city.transports.include?(options['transport']) # "type,number"
				city.transports << options['transport']
				city.save
				city.inc(:transports_count,1) 
			end
		end
	end


end