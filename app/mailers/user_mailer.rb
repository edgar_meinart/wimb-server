class UserMailer < ActionMailer::Base
  default from: "reviews@wimb.com"

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.user_mailer.review_mail.subject
  #
  def review_mail(info = {})
    @info = info
    mail to: "edgars@meinarts.name", subject: "Review, #{info[:category]}"
  end
end
