# 1000000.times do |i|
# 	Sidekiq::Client.enqueue(UserCreate)	
# end
cities = []
10.times do
	cities << Faker::Address.city
end

N = 100
N.times do |i|
	city = cities[rand(cities.size)]
	username = Faker::Name.name
	phone_number = Faker::PhoneNumber.phone_number
	device_id = "#{rand(N+100)}-i"
	email = Faker::Internet.email
	rank = rand(100000)
	User.create(city: city, username: username, phone_number: phone_number, device_id: device_id, email: email, rank: rank)
end
